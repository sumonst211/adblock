- **Release:** (Include link to the milestone that is to be released)
- **Version:** (Release version string)

## QA checklist

### Pre-testing

- [ ] regression tests are updated ([cucumber project](https://studio.cucumber.io/projects/283030)) _[@person_responsible]_
- [ ] testpages autotests are passing _[@person_responsible]_
- [ ] E2E autotests are passing _[@person_responsible]_
- [ ] test runs are created _[@person_responsible]_

### Release testing

- [ ] **chrome latest** (Link to testrun) _[@person_responsible]_
- [ ] **firefox latest** (Link to testrun) _[@person_responsible]_
- [ ] **edge latest** (Link to testrun) _[@person_responsible]_
- [ ] **chrome minimum** (Link to testrun) _[@person_responsible]_
- [ ] **firefox minimum** (Link to testrun) _[@person_responsible]_
- [ ] **chrome misc** (Link to testrun) _[@person_responsible]_


/label ~"Product:: AdBlock"
